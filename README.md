# success-status-codes-example

**Fork this project into a namespace with an Ultimate license for this example to work.**

This example project performs a dynamic-analysis security test
against a REST target application built using Python and Flask.

For demonstration purposes, we have two har files:

- `rest_target_http_archive_with_token.har` - a request specification to create a user and includes an Authorization header in the request body
  - we expect making this request should return a `201` response status code.
- `rest_target_http_archive_no_token.har` - a request specification to create a user and does **not** include an Authorization header in the request body
  - we expect making this request should return a `401` response status code due to authentication failure.

The [APISEC_SUCCESS_STATUS_CODES](https://docs.gitlab.com/ee/user/application_security/api_security_testing/configuration/variables.html) CI/CD variable enables one to specify a list of HTTP status codes that helps control whether the security test scanning job has passed or failed based on the tested request responses. For this example we have specified our success status codes to be `200, 201, 204`.

If we run the pipeline with `rest_target_http_archive_with_token.har`, we can see that the `api_security` job passes as the tested operation includes a request response whose code matches what we have listed in `APISEC_SUCCESS_STATUS_CODES`:

**Example configuration:**

```log
variables:
    APISEC_HAR: rest_target_http_archive_with_token.har
    APISEC_SUCCESS_STATUS_CODES: '200, 201, 204'
    APISEC_TARGET_URL: http://target:7777
```

**Example output with correct status code:**

![alt text](image.png)

If we keep the same list of success status codes but interchange the HAR file to `rest_target_http_archive_no_token.har` and run the pipeline, we should now see that the `api_security` job has failed:

**Example configuration:**

```log
variables:
    APISEC_HAR: rest_target_http_archive_no_token.har
    APISEC_SUCCESS_STATUS_CODES: '200, 201, 204'
    APISEC_TARGET_URL: http://target:7777
```

**Example output with authentication failure:**

![alt text](image-1.png)
